<?php

/**
 * @file
 * Contains \Drupal\taxonomy_block\Form\TaxonomyBlockForm.
 */

namespace Drupal\taxonomy_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;

class TaxonomyBlockForm extends ConfigFormBase {

  const settings = 'taxonomy_block.settings';

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'taxonomy_block_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config(self::settings);

    $vocabularies = Vocabulary::loadMultiple();

    $options = array();
    foreach ($vocabularies as $vocabularie) {
      $id = $vocabularie->id();
      $options[$id] = $id;
      //dpm($vocabularie);
    }

    $selected_vocabularies = $config->get('vocabularies');

    $form['vocabularies'] = array(
      '#type' => 'select',
      '#title' => ('Vocabularies'),
      '#options' => $options,
      '#default_value' => $selected_vocabularies,
      '#multiple' => TRUE,
      '#description' => "Do not select any itmes if you want to show all vocabulairies.",
    );


    $form['style.class'] = array(
      '#type' => 'textfield',
      '#title' => ('CSS class'),
      '#default_value' => $config->get('style.class')
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (strpos($form_state->getValue('email'), '.com') === FALSE) {
      //$form_state->setErrorByName('email', $this->t('This is not a .com email address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config(self::settings);
    $config->set('style.class', $form_state->getValue('style.class'));
    $config->set('vocabularies', $form_state->getValue('vocabularies'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::settings];
//    return [
//      'taxonomy_block.settings',
//    ];
  }

}
