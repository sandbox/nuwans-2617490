<?php

namespace Drupal\taxonomy_block\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy_block\Form\TaxonomyBlockForm;

/**
 * Provides a 'Taxonomy Block' block.
 *
 * @Block(
 *   id = "taxonomy_block",
 *   admin_label = @Translation("Tags"),
 * )
 */
class TaxonomyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = array();
    $output[]['#cache']['max-age'] = 0;
    $output[]['#attached']['library'][] = 'taxonomy_block/taxonomy_block-styling';

    $config = Drupal::config(TaxonomyBlockForm::settings);

    $vids = $config->get('vocabularies');
    $class = $config->get('style.class');
    if ($vids && is_array($vids) && count($vids)) {
      $query = Drupal::entityQuery('taxonomy_term');
      $query_or = $query->orConditionGroup();
      foreach ($vids as $vid) {
        $query_or->condition('vid', $vid);
      }
      $query->condition($query_or);
      $tids = $query->execute();
      $terms = Term::loadMultiple($tids);
    }
    else {
      $terms = Term::loadMultiple();
    }





    $html = "";
    $html .= "";
    foreach ($terms as $term) {
      $link = Drupal::l($term->toLink()->getText(), $term->toUrl());
      $html .= " <span class='$class'>$link</span> ";
    }
    if ($html) {
      $output[] = ['#markup' => $html];
    }

    return $output;
  }

}
